﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ListPractice
{
    class FileHandleCode
    {
        public void writefilehandle()
        {
            StreamWriter sWriter = new StreamWriter("C:/Users/aashishj/Desktop/file.txt");
            Console.WriteLine("Enter the text:");
            string text = Console.ReadLine();
            sWriter.WriteLine(text);
            
            sWriter.Close();
        }


        public void readfilehandle()
        {
            StreamReader sReader = new StreamReader("C:/Users/aashishj/Desktop/file.txt");
            Console.WriteLine("Read the file:");
            sReader.BaseStream.Seek(2, SeekOrigin.End);
            string test = sReader.ReadLine();
            while(test!=null)
            {
                Console.WriteLine(test);
                test = sReader.ReadLine();
            }
            Console.ReadLine();
            sReader.Close();
        }


    }
}
