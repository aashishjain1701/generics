﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListPractice
{
    enum months
    {
        january,
        february,
        march,
        april,
        may,
        june,
        july,
        august,
        september,
        october,
        november,
        december
    }
    class Project
    {

        public static void Main(string[] args)
        {
            // IntegerSum result = new IntegerSum();
            // result.sumOfIntegers();

            // RemoveSpaces results = new RemoveSpaces();
            //results.RemoveDuplicateSpaces();

            //StringLength resultt = new StringLength();
            //resultt.stringLengthCount();




            //Task #2 : Create a class called "Person" with two properties, Name and Age
            // Create a List of Person type and populate it with 5 sample persons
            // Display all the name of the persons along with age whose age is greater than 60 for example.

            /*
            Person firstPerson = new Person();
            firstPerson.Name = "Aashish";
            firstPerson.Age = 22;


            Person secondPerson = new Person();
            secondPerson.Name = "Arif";
            secondPerson.Age = 65;


            Person thirdPerson = new Person();
            thirdPerson.Name = "Ayush";
            thirdPerson.Age = 125;


            Person fourthPerson = new Person();
            fourthPerson.Name = "Anand";
            fourthPerson.Age = 32;


            Person fifthPerson = new Person();
            fifthPerson.Name = "Prabhat";
            fifthPerson.Age = 20;


            List<Person> Mylist = new List<Person>();
            Mylist.Add(firstPerson);
            Mylist.Add(secondPerson);
            Mylist.Add(thirdPerson);
            Mylist.Add(fourthPerson);
            Mylist.Add(fifthPerson);

            foreach(var person in Mylist)
            {
                if(person.Age>60)
                {
                    Console.WriteLine("{0}, {1}", person.Name, person.Age);
                }
            }
            */





            //Task #3 : Create an enum for Months.
            // Store the number of days against a month in a Dictionary.
            // Create a method which will take the month and will return its days.

            /*
             Dictionary<string,int> MyDictionary = new Dictionary<string,int>();

             MyDictionary.Add("january",31);
             MyDictionary.Add("february", 28);
             MyDictionary.Add("march", 31);
             MyDictionary.Add("april", 30);
             MyDictionary.Add("may", 31);
             MyDictionary.Add("june", 30);
             MyDictionary.Add("july", 31);
             MyDictionary.Add("august", 31);
             MyDictionary.Add("september", 30);
             MyDictionary.Add("october", 31);
             MyDictionary.Add("november", 30);
             MyDictionary.Add("december", 31);


             foreach (KeyValuePair<string,int> ele1 in MyDictionary)
             {
                 Console.WriteLine("{0} {1}",
                           ele1.Key, ele1.Value);
             }
            Console.WriteLine("\nEnter the month name to get the no of days:");
            string months = Console.ReadLine();
            months = months.ToLower();
            NoOfDays(MyDictionary,months);
            Console.ReadKey();
            */




            //Exception Handling. Using Try, Catch and Finally

            // ExceptionHandling obj = new ExceptionHandling();
            //obj.ExceptionHandle();



            // File Handling Code

            // FileHandleCode fhc = new FileHandleCode();

            //this is the stream writer implementation for writting in the file.

            //fhc.writefilehandle();
            //Console.ReadKey();



            //This is the stream reader implementation for reading the file.

            //fhc.readfilehandle();
            //Console.ReadKey();




        }

        public static void NoOfDays(Dictionary<string, int> MyDictionary,string month)
        {

            foreach (KeyValuePair<string, int> ele2 in MyDictionary)
            {
                if (ele2.Key == month)
                {
                    Console.WriteLine("Number of Days:" + ele2.Value);
                }
           
                
            }
        }
    }
}
