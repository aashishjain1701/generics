﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListPractice
{
    class IntegerSum
    {
        public void sumOfIntegers()
        {
            int index,temp;
            string input;
            int sum = 0;
            input = Console.ReadLine();
            int length = input.Length;
            for (index=0;index<=length-1;index++)
            {
                if(input[index]>=48 && input[index]<=57)
                {
                    temp = input[index] - 48;
                    sum += temp;
                }
            }
            Console.WriteLine("sum of integers in the string is: {0}", sum);
        }
    }
}
