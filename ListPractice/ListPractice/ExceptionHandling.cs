﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListPractice
{
    class ExceptionHandling
    {
        public void ExceptionHandle()
        {
            try
            {
                int first = 23;
                int second = 0;
                Console.WriteLine("This is the try Block:::::::::::::::::::::::");
                int result = first / second;

            }
            catch(DivideByZeroException e)
            {
                Console.WriteLine("This is the catch block :::::::::::::\n{0}\n\n",e);
            }
            finally
            {
                Console.WriteLine("'.'.'.'.'.'.'.'.'This is finally Block, " +
                    "which will be executed whether the exception is handled or not'.'.'.'.'.'.'.'.'");
            }
        }
    }
}
