﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListPractice
{
    
    class Program
    {
        static void ain(string[] args)
        {
            List<int> firstList = new List<int>();
            firstList.Add(23);
            firstList.Add(43);
            firstList.Add(31);
            firstList.Add(57);
            firstList.Add(89);
            
            Console.Write("\nFirst list initial count:");
            Console.WriteLine(firstList.Count());
            Console.WriteLine("\nFirst list intially:");
            
            int index = 0;
            foreach (int key in firstList)
            {
                Console.WriteLine(key);
                index++;
            }
            Console.WriteLine("\nChecking if element 34 present in list or not:");
            Console.WriteLine(firstList.Contains(34));
            List<int> secondList = new List<int>(firstList);
            
            Console.WriteLine("\nsecond list initial count:{0} ",secondList.Count());
            Console.WriteLine("\nsecond list initially");
            foreach (int key in secondList)
            {
                Console.WriteLine(key);
                index++;
            }
            Console.WriteLine("\nremoving element 43 ",firstList.Remove(43));
            Console.WriteLine("\nfirst list after removing an element");
            foreach (int key in firstList)
            {
                Console.WriteLine(key);
                index++;
            }
            Console.WriteLine("\nsecond list afer removing element from first list ");
            foreach (int key in secondList)
            {
                Console.WriteLine(key);
                index++;
            }
            Console.WriteLine("\nfirst list count: {0}", firstList.Count());
            Console.WriteLine("second list counts: {0}",secondList.Count());

            ObservableCollection<int> thirdList = new ObservableCollection<int>();
            thirdList.Add(7);
            thirdList.Add(5);
            thirdList.Add(53);
            thirdList.Add(15);
            thirdList.Add(98);

            Console.WriteLine("\nthird list(observable Collection) initial count:{0}", thirdList.Count());
            foreach (int key in thirdList)
            {
                Console.WriteLine(key);
                index++;
            }
            ObservableCollection<int> fourthList = thirdList;

            Console.WriteLine("\nFourth list(Observable Collection) initial count:{0}", fourthList.Count());
            foreach (int key in fourthList)
            {
                Console.WriteLine(key);
                index++;
            }

            Console.WriteLine("\nRemoving element 5");
            thirdList.Remove(5);

            Console.WriteLine("\nThird list(Observable Collection) count after removing an element:{0}", thirdList.Count());
            foreach (int key in thirdList)
            {
                Console.WriteLine(key);
                index++;
            }

            Console.WriteLine("\nFourth list(Observable Collection) count after removing an element from list third:{0}", fourthList.Count());
            foreach (int key in fourthList)
            {
                Console.WriteLine(key);
                index++;
            }

           
        }
    }
}
