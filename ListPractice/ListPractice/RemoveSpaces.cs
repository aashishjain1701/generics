﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListPractice
{
    class RemoveSpaces
    {
        public void RemoveDuplicateSpaces()
        {
            int index;
            string spaces;
            string output = "";
            Console.WriteLine("Enter the string:");
            spaces = Console.ReadLine();
            spaces.Trim();
            Console.WriteLine("\nString : {0}", spaces);

            int length = spaces.Length;

            for (index = 1; index < length; index++)
            {
                if (spaces[index - 1] == spaces[index] && spaces[index] == 32 && spaces[index] != '\0')
                {
                    continue;
                }
                else
                {
                    output += spaces[index];
                }
            }
            Console.WriteLine("result:: '{0}'", output);

        }
    }
}
